// Add more textarea IDs that can parse Video Filter.
var textareaIDs = {
  "edit-teaser-js": "",
  "edit-body": "",
  "edit-comment": ""
};

var textareaFocussed = "edit-body";

Drupal.behaviors.VideoFilterFocus = function() {
  $("textarea").bind("focus", function() {
    var textAreaID = $(this).attr("id");
    if (textAreaID in textareaIDs) {
      textareaFocussed = textAreaID;
    }
  });
}

function addVideoFilter() {
	var videotag = $(".video-filter-box #edit-input-box").val();

	if (videotag != "") {
		videotag = "[video:" + videotag + "]";
		$("textarea#"+ textareaFocussed +", textarea#edit-comment").each(function() {
			// Plain textarea support
			if (document.selection) {
				this.focus();
				document.selection.createRange().text = videotag;
			}
			else if (this.selectionStart || this.selectionStart === 0) {
				var cursorPos = this.selectionEnd + videotag.length;
				this.value = this.value.substring(0, this.selectionStart) + videotag + this.value.substring(this.selectionEnd);
				this.selectionStart = this.selectionEnd = cursorPos;
			}
			else {
				this.value = this.value + videotag;
			}
			this.focus();
		});
	}
}
